﻿//#r "System.Windows.Forms.dll"
//#r "fiddler/FiddlerCore.dll"

using Fiddler;
using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

public class Startup
{
    Func<object, Task<object>> _console;
    Func<object, Task<object>> _html;

    public async void _print(object text){
        if(_console!=null)
            await _console(text);
    }
    
    public async void _getHtml(object html)
    {
        if (_html != null)
            await _html(html);
    }

    public async Task<object> Invoke(dynamic data)
    {
        _console = (Func<object, Task<object>>)data.console;
        _html = (Func<object, Task<object>>)data._html;

        _print("Started");
        FiddlerApplication.Shutdown();
        new FiddlerLogic
        {
            _beforeRequest = (oS) => {
                var proxy = oS.oRequest.headers["POverride"];
                if (proxy != null)
                {
                    oS["X-OverrideGateway"] = proxy;
                }
            },
            _beforeResponse = (oS) =>
            {
                var response = oS.GetResponseBodyAsString();
                _html(response);
            }
        }._start(5000);
        return Task.FromResult("Done");
    }
}


class FiddlerLogic
{
    public Action<Session> _beforeRequest;
    public Action<Session> _beforeResponse;

    public void _start(int port=5555)
    {
        FiddlerApplication.BeforeRequest += (oS) => _beforeRequest(oS);
        FiddlerApplication.BeforeResponse += (oS) => _beforeResponse(oS);

        FiddlerApplication.Startup(port, false, true);
    }
}